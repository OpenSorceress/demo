<?php

class Message extends Eloquent
{
	protected $table = 'messages';
	protected $hidden = array('user_id', 'channel_id');

	public function users()  {
		return $this->belongsTo("User");
	}

	public function channels()
	{
		return $this->belongsTo("Channel");
	}

}