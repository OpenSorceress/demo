<?php

class Server extends Eloquent
{
	protected $table = 'servers';
	protected $hidden = array('id');

	public function users()  {
		return $this->hasMany("User");
	}

	public function ops() {
		return $this->morphMany("Op", "scope");
	}

	public function channels()
	{
		return $this->hasMany('Channel');
	}
}