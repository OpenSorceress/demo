<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;
use Illuminate\Database\Schema\Grammars;
use Illuminate\Database\Connectors;
use Doctrine\DBAL;
use Doctrine\Tests\Common\Annotations\Ticket\Doctrine\ORM\Mapping;


class User extends Eloquent implements UserInterface, RemindableInterface {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	protected $schema = array();
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'id');

	protected $fillable = array('first_name', 'last_name', 'email', 'ip', 'cloak');

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}

	public function getSchema()
	{
		foreach(DB::getDoctrineSchemaManager()->listTableColumns($this->getTable()) as $key=>$val) {
		   $this->schema[$key]['name'] = $key;
			$this->schema[$key]['definition'] = $val->getType()->getName();
		}
		return $this->schema;
	}

	public function messages()
	{
		return $this->hasMany('Message');
	}

	public function nicks()
	{
		return $this->hasMany('Nick');
	}

}