<?php

class Nick extends Eloquent
{
	protected $table = 'nicks';
	protected $hidden = array();
	protected $fillable = array('id', 'user_id', 'server_id', 'message_id', 'nick', 'ip', 'host', 'cloak');
	protected $guarded = array();


	public function users()  {
		return $this->belongsTo("User");
	}

	public function messages()
	{
		return $this->hasMany("Message");
	}

	public function servers()
	{
		return $this->belongsTo('Server');
	}

}