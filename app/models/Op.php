<?php

class Op extends Eloquent
{
	protected $table = 'ops';
	protected $hidden = array('id', 'user_id', 'channel_id', 'server_id');
	protected $fillable = array('id', 'user_id', 'channel_id', 'server_id');

	public function users()  {
		return $this->hasOne("User");
	}

	public function scope()
	{
		return $this->morphTo();
	}

}