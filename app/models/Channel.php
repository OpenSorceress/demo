<?php

class Channels extends Eloquent
{
	protected $table = 'channels';
	protected $hidden = array('user_id', 'message_id', 'server_id');
	protected $fillable = array('id', 'server_id', 'name', 'motd');
	protected $guarded = array();

	public function users()  {
		return $this->hasMany("User");
	}

	/**
	 * Polymorphic model relationship ops
	 *
	 * @uses \Illuminate\Database\Eloquent\Model::Op
	 * @return Eloquent|\Illuminate\Database\Eloquent\Relations\MorphMany
	 */
	public function ops() {
		return $this->morphMany("Op", "scope");
	}

	public function messages()
	{
		return $this->hasMany("Message");
	}

	public function servers()
	{
		return $this->belongsTo('Server');
	}

}