@extends('layouts.template')

@section('content')

@if(Auth::check())
    <div class="login"><a class="logout" href="logout">logout {{ Auth::getUser()->name }}</a></div>
@else
    <div class="login"><a class="login" href="#">login</a></div>
@endif

<table>
	<thead>
	<tr>
		<th>Id</th>
		<th>Nick</th>
		<th>Username</th>
		<th>Email</th>
		<th>Server Name</th>
	</tr>
	</thead>
	<tbody>
	<tr>
		<td>{{ $nick->id }}</td>
		<td>{{ $nick->nick }}</td>
		<td>{{ $nick->users->username; }}</td>
		<td>{{ $nick->users->email; }}</td>
		<td>{{ $nick->servers->name; }}</td>
	</tr>
	</tbody>
</table>
@stop