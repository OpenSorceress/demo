@extends('layouts.template')

@section('content')
<h2>Upsert: create or update User model</h2>
<div class="form">
{{ Form::open(array('url' => 'users/upsert')) }}
<?php foreach($fields as $name=>$field): ?>
    <div>
	    @if(isset($user) && !is_null($user))
			@if ($name == 'id')
		        {{ Form::hidden($name, $user->id, array('class' => $field['definition'])) }}
			@else
	            {{ Form::label($name, null, array('class' => $field['definition'])) }}
		        @if ($name !== 'password')
			        {{ Form::text($name, $user->$name, array('class' => $field['definition'])) }}
		        @elseif ($name === 'nickserv')
			        {{ Form::select($name, array(0=>'Unregistered',1=>'Registered'), array('class' => $field['definition'])) }}
		        @else
			        {{ Form::label($name, null, array('class' => $field['definition'])) }}
		            {{ Form::password($name, null, array('class' => $field['definition'])) }}
	            @endif
	        @endif
	    @else
		    @if ($name !== 'id' && $name !== 'created_at' && $name !== 'updated_at')
			        {{ Form::label($name, null, array('class' => $field['definition'])) }}
				@if ($name === 'password')
				        {{ Form::password($name, null, array('class' => $field['definition'])) }}
				@elseif ($name === 'nickserv')
				        {{ Form::select($name, array(0=>'Unregistered',1=>'Registered'), array('class' => $field['definition'])) }}
				@elseif ($name != 'created_at' && $name != 'updated_at')
				        {{ Form::text($name, null, array('class' => $field['definition'])) }}
			    @endif
	        @endif
	    @endif
    </div>
<?php endforeach; ?>
{{ Form::submit('Upsert!'); }}
{{ Form::close() }}
</div>
@stop