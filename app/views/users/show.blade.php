@extends('layouts.template')

@section('content')

@if(Auth::check())
    <div class="login"><a class="logout" href="logout">logout {{ $user->username }}</a></div>
@else
    <div class="login"><a class="login" href="#">login</a></div>
@endif

<table>
	<thead>
	<tr>
		<th>Id</th>
		<th>Username</th>
		<th>Email</th>
	</tr>
	</thead>
	<tbody>
	<tr>
		<td>{{ $user->id }}</td>
		<td>{{ $user->username }}</td>
		<td>{{ $user->email }}</td>
	</tr>
	</tbody>
</table>
@stop