@extends('layouts.template')

@section('content')
<h2>Upsert</h2>
<div class="form">
{{ Form::open() }}
@foreach($fields as $field)
    <div>
	    @if ($field->name !== 'id')
		    @if ($field->name == 'password')
		        {{ Form::label($field->name, null, array('class' => $field->definition)) }}
		        {{ Form::password($field->name, null, array('class' => $field->definition)) }}
		    @elseif ($field->name != 'created_at' && $field->name != 'updated_at')
				{{ Form::label($field->name, null, array('class' => $field->definition)) }}
				{{ Form::text($field->name, null, array('class' => $field->definition)) }}
	    @endif
	    @endif
    </div>
@endforeach
{{ Form::submit('Create user!'); }}
{{ Form::close() }}
</div>
@stop