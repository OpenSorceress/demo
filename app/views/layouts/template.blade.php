<!doctype html>
<html>
<head>
	{{ HTML::style('css/bootstrap.css') }}
	{{ HTML::style('css/bootstrap-responsive.css') }}
	{{ HTML::style('css/common.css'); }}
	{{ HTML::style('css/content.css'); }}

</head>
<body>
	<div id="wrapper">
		<div class="container">
			<div id="header">

			</div>
		</div>
		<div class="container">
			<div id="content">
			@yield('content')
			</div>
		</div>
	</div>
	{{ HTML::script('https://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js') }}
	{{ HTML::script('js/bootstrap.min.js') }}
	{{ HTML::script('js/script.js') }}
</body>
</html>