@extends('layouts.template')

@section('content')
@if (isset($auth))
        Logged in!
@else
<div class="login">
<h2>Login</h2>
{{ Form::open(array('login')) }}
<div>
{{ Form::label('username', null, array('class'=>'text')) }}
{{ Form::text('username', null, array('class'=>'text')) }}
</div>
<div>
{{ Form::label('password', null, array('class' => 'password')) }}
{{ Form::password('password', null, array('class' => 'password')) }}
</div>
{{ Form::submit('Login!'); }}
{{ Form::close() }}
</div>
@endif
@stop