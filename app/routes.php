<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('template', function() {
	return View::make('template');
});

Route::get('content', function() {
	return View::make('content');
});

Route::model('user', 'User');
Route::model('app', 'App');
Route::model('nick', 'Nick');
Route::model('server', 'Server');
Route::model('channel', 'Channel');
Route::model('message', 'Message');
Route::model('op', 'Op');

Route::get('login', function() { return View::make('login'); });

Route::post('login', array('as'=> 'login', 'do'=> function(){
	$username = Input::get('username');
	$password = Input::get('password');
	$authAr = array(
		'username' => $username,
		'password' => $password
	);
	if ($user = Auth::attempt($authAr)) {
		return Redirect::secure('/users/view/'.Auth::getUser()->id);
	} else {
		return Redirect::to('login')->with('login_errors',true);
	}
}));
Route::get('logout', function() { if(Auth::logout()) return Redirect::secure('login'); });

Route::controller('users', 'UserController');
Route::controller('ops', 'OpController');
Route::controller('servers', 'ServerController');
Route::controller('channels', 'ChannelController');
Route::controller('messages', 'MessageController');
Route::controller('nicks', 'NickController');

Route::get('{appSlug}', 'AppController@getView');
Route::post('{appSlug}', 'AppController@postView');

# Index Page - Last route, no matches
Route::get('/', 'AppController@getIndex');