<?php

use Illuminate\Support\Facades\Redirect;

class ServerController extends BaseController
{

	protected $layout = 'layouts.template';
	protected $data = array();
	protected $fillable = array('first_name', 'last_name', 'email');
	protected $guarded = array('id', 'password');

	public function __construct()
	{
		$this->Server = new Server;
	}

}