<?php

use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\RedirectResponse;

/**
 * Class UserController
 *
 * @uses BaseController
 */
class UserController extends BaseController {

	protected $layout = 'layouts.template';
	protected $data = array();
	protected $fillable = array('first_name', 'last_name', 'email');
	protected $guarded = array('password');

	public function __construct()
	{
		$this->User = new User;
		$this->data = Input::get('user');
		if (count($this->data) > 0) {
			foreach($this->data as $key=>$val) {
				if ($key !== '_token') {
					if($key == 'password') {
						$val = Hash::make($val);
					}
					$this->data[$key] = $val;
				}
			}
		}

		$this->beforeFilter('csrf', array('on' => 'post'));
		$this->afterFilter('log', array('only' => array('indexAction', 'viewAction')));
	}

	public function getIndex()
	{
		$users = User::all();
		return View::make("users.index")->with('users', $users);
	}

	public function getView($id)
	{
		$user = User::find($id)->with(array('nicks'));
		return View::make("users.show")->with('user', $user);
	}

	public function getUpsert($id = null)
	{
		if (isset($id) && !is_null($id)) {
			$user = User::find($id);
			return View::make('users.upsert', array('fields'=>$this->User->getSchema()))->with('user', $user);
		}
		return View::make('users.upsert', array('fields'=>$this->User->getSchema()));
	}

	public function postUpsert($id = null)
	{
		Input::all();
		if (!isset($id) || is_null($id)) {
				$method = 'insertGetId';
				$date = date("Y-m-j H:i:s",time());
				$this->data = array('created_at'=>$date,'updated_at'=>$date);
			} else {
				$method = 'update';
			}
			foreach(Input::all() as $key=>$val) {
				if($key != '_token'){
					if ($key == 'password') {
						$val = Hash::make($val);
					}
					$this->data[$key] = $val;
			}
		}
		if ($method == 'update') {
			$this->User->where('id', $id)->update($this->data);
		} else {
			$id = $this->User->insertGetId($this->data);
		}
	    return Redirect::secure("users/view/$id");
	}



}