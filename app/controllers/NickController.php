<?php

use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\RedirectResponse;

/**
 * Class NickController
 *
 * @uses BaseController
 */
class NickController extends BaseController
{

	protected $layout = 'layouts.template';
	protected $fillable = array();
	protected $guarded = array('password');
	protected $relations = array('server', 'user');

	public function __construct()
	{

	}

	public function getIndex()
	{
		$nicks = Nick::with('users','servers')->get();
		return View::make("nicks.index",array('nicks'=>$nicks));
	}

	public function getView($id)
	{
		$nick = Nick::find($id)->with(array(
			'users'=>function($nick) {
				return $nick->get();
			},
			'servers'=>function($nick) {
				return $nick->get();
			}))
		->first();
		return View::make("nicks.show")->with('nick', $nick);
	}

	public function getUpsert($id = null)
	{
		if (isset($id) && !is_null($id)) {
			$nick = Nick::find($id);
			return View::make('nicks.upsert', array('fields'=>Nick::getSchema()))->with('nick', $nick);
		}
		return View::make('nicks.upsert', array('fields'=>Nick::getSchema()));
	}

	public function postUpsert($id = null)
	{
		Input::all();
		if (!isset($id) || is_null($id)) {
			$method = 'insertGetId';
			$date = date("Y-m-j H:i:s",time());
			$this->data = array('created_at'=>$date,'updated_at'=>$date);
		} else {
			$method = 'update';
		}
		foreach(Input::all() as $key=>$val) {
			if($key != '_token'){
				if ($key == 'password') {
					$val = Hash::make($val);
				}
				$this->data[$key] = $val;
			}
		}
		if ($method == 'update') {
			Nick::where('id', $id)->update($this->data);
		} else {
			$id = Nick::insertGetId($this->data);
		}
		return Redirect::secure("nicks/view/$id");
	}

}