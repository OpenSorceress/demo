<?php

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Monolog\Formatter\LineFormatter;

class AppController extends BaseController {

	/**
	 * App Model
	 * @var app
	 */
	protected $app;


	/**
	 * Inject the models.
	 * @param app $app
	 * @param User $user
	 */
	public function __construct(App $app)
	{
		$this->app = $app;
		$this->beforeFilter('auth');
	}

	public function getIndex()
	{
		return View::make('hello');
	}

}