<?php

use Illuminate\Database\Migrations\Migration;

class CreateOpsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ops', function($table) {
			$table->increments('id');
			$table->string('nick_id');
			$table->string('channel_id');
			$table->string('server_id');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ops');
	}

}