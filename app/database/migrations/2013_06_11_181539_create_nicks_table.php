<?php

use Illuminate\Database\Migrations\Migration;

class CreateNicksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nicks', function($table) {
			$table->increments('id');
			$table->string('user_id');
			$table->string('server_id');
			$table->string('nick');
			$table->string('ip');
			$table->string('host');
			$table->string('cloak');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nicks');
	}

}