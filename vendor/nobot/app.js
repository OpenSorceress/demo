/**
 * this is my cutesy little irc bot.
 * @type {{server: string, autoRejoin: boolean, floodProtection: boolean, nickMod: boolean, channels: Array, botName: string}}
 */

var irc = require('irc');
var fs = require('fs');

var package = require('./package.json');
var config = package.config;

var client = new irc.Client(config.server, config.botName, { channels: config.channels });
var file = 'message.' + (function(date) { return date.getTime(); })(new Date) + '.txt';

client.addListener('message', function (from, to, message) {
    fs.appendFile(file, '[' + to + '] ' + from + ": " + message + "\n", function (err) {
        if (err) console.log(err);
    });
});

client.addListener('join', function(from, message) {
    if (from == '') {
        client.whois(message,  function(data) {
            fs.appendFile(file, '[' + from + '] ' + message + " joined\n" +
                (function(data) {
                    var str = "";
                    for(var obj in data) {
                        str += "\t" + obj + ": " + data[obj] + "\n";
                    }
                    return str;
                })(data) + "\n", function (err) {
                if (err) console.log(err);
            });
        });
        client.notice(message, "this channel is logged.");
    } else {
        fs.appendFile(file, '[' + from + '] ' + message + " joined\n", function (err) {
            if (err) console.log(err);
        });
    }
});

client.addListener('action', function(from, to, message) {
    fs.appendFile(file, '[' + to + '] ' + from + " " + message + "\n", function (err) {
        if (err) console.log(err);
    });
});

client.addListener('error', function(message) {
    fs.appendFile(file, '[ Error ] ' + message + "\n", function (err) {
        if (err) console.log(err);
    });
});