$(document).ready(function() {

    $(':submit').on("click", function() {
        var formData = $(this).parents('form').serialize();
        $.ajax({
                url:'users/login',
                data: formData,
                type:"POST",
                success: console.log.bind(console),
                error:   console.log.bind(console)
            });
    });

});